#[cfg(test)]
mod tests;

use std::mem;
use std::ptr;

const PAGE_SIZE: usize = 4096;
const LEAF_MAX_VALUES: usize = 4;
const INTERNAL_MAX_KEYS: usize = 4;

struct InternalNode {
    parent: *mut Node,
    cells: Vec<(u32, *mut Node)>,
    right_child: *mut Node,
}

struct LeafNode {
    parent: *mut Node,
    next_leaf: *mut LeafNode,
    cells: Vec<(u32, u64)>,
}

enum Node {
    Internal(InternalNode),
    Leaf(LeafNode),
}

fn bsearch_left<K: PartialOrd, V>(array: &[(K, V)], e: K) -> usize {
    let mut min_index = 0;
    let mut max_index = array.len();
    while min_index != max_index {
        let index = (min_index + max_index) / 2;
        if array[index].0 >= e {
            max_index = index;
        } else {
            min_index = index + 1;
        }
    }
    min_index
}

fn bsearch_right<K: PartialOrd, V>(array: &[(K, V)], e: K) -> usize {
    let mut min_index = 0;
    let mut max_index = array.len();
    while min_index != max_index {
        let index = (min_index + max_index) / 2;
        if array[index].0 > e {
            max_index = index;
        } else {
            min_index = index + 1;
        }
    }
    min_index
}

impl Node {
    fn from_mem(buffer: Box<[u8; PAGE_SIZE]>) -> Node {
        let type_name = &buffer[0..4];
        if type_name == b"bpti" { // B+Tree internal
            Node::Internal(unimplemented!())
        } else if type_name == b"bptl" { // B+Tree leaf
            Node::Leaf(unimplemented!())
        } else {
            panic!("Unknown node type")
        }
    }

    fn leaf(&mut self) -> Option<&mut LeafNode> {
        match self {
            &mut Node::Internal(_) => None,
            &mut Node::Leaf(ref mut leaf) => Some(leaf),
        }
    }

    fn internal(&mut self) -> Option<&mut InternalNode> {
        match self {
            &mut Node::Internal(ref mut i) => Some(i),
            &mut Node::Leaf(_) => None,
        }
    }
}

pub struct BTree {
    root: *mut Node,
}

impl BTree {
    pub fn new() -> BTree {
        let mut root = Box::new(
            Node::Leaf(LeafNode {
                parent: ptr::null_mut(),
                next_leaf: ptr::null_mut(),
                cells: Vec::new(),
            })
        );
        let root_ptr = &mut *root as *mut Node;
        mem::forget(root);
        BTree {
            root: root_ptr,
        }
    }

    fn search_internal(&mut self, key: u32) -> (*mut LeafNode, usize) {
        let mut node = unsafe { &mut *self.root };
        loop {
            enum Ret<'b> {
                Descend(&'b mut Node),
                Return(*mut LeafNode, usize),
            }
            let ret = match node {
                &mut Node::Internal(InternalNode {
                    ref mut cells,
                    right_child,
                    ..
                }) => {
                    let index = bsearch_right(cells, key);
                    let ptr: *mut Node = if index == cells.len() {
                        right_child
                    } else {
                        cells[index].1
                    };
                    Ret::Descend(unsafe { &mut *ptr })
                }
                &mut Node::Leaf(ref mut leaf) => {
                    let index = {
                        let LeafNode { ref mut cells, .. } = leaf;
                        bsearch_left(cells, key)
                    };
                    Ret::Return(leaf, index)
                }
            };
            match ret {
                Ret::Descend(n) => node = n,
                Ret::Return(leaf, index) => {
                    return (leaf, index);
                }
            }
        }
    }

    pub fn search<'a>(&'a mut self, key: u32) -> Cursor<'a> {
        let (node, index) = self.search_internal(key);
        Cursor {
            leaf: unsafe { &mut *node },
            index: index,
        }
    }

    pub fn insert(&mut self, key: u32, value: u64) {
        let (leaf, index) = self.search_internal(key);
        let leaf = unsafe { &mut *leaf };
        if index < leaf.cells.len() && leaf.cells[index].0 == key {
            // Replace value of existing key
            leaf.cells[index].1 = value;
            return;
        } else if leaf.cells.len() + 1 < LEAF_MAX_VALUES {
            // Add this value to the leaf
            leaf.cells.insert(index, (key, value));
            return;
        }

        // Split the leaf
        let middle = leaf.cells.len() / 2;
        let new_key = leaf.cells[middle - 1].0;
        // Create new leaf
        let mut new_node = {
            let mut node = Box::new(Node::Leaf(LeafNode {
                parent: leaf.parent,
                next_leaf: leaf.next_leaf,
                cells: Vec::new(),
            }));
            let node_ptr = &mut *node as *mut Node;
            mem::forget(node);
            node_ptr
        };
        let right_leaf = unsafe { &mut *new_node }.leaf().unwrap();
        leaf.next_leaf = right_leaf;
        // Move data over
        right_leaf.cells.extend_from_slice(&leaf.cells[middle..]);
        leaf.cells.truncate(middle);
        // Insert new value
        if index >= middle {
            right_leaf.cells.insert(index - middle, (key, value));
        } else {
            leaf.cells.insert(index, (key, value));
        }

        // Insert into the internal nodes, splitting them if necessary
        let mut node = unsafe { &mut *leaf.parent }.internal().unwrap();
        loop {
            if node.cells.len() + 1 < INTERNAL_MAX_KEYS {
                let index = bsearch_left(&node.cells, new_key);
                // Insert new key
                if index < node.cells.len() {
                    node.cells.insert(index, (new_key, new_node));
                    let tmp = node.cells[index].1;
                    node.cells[index].1 = node.cells[index + 1].1;
                    node.cells[index + 1].1 = tmp;
                } else {
                    node.cells.push((new_key, node.right_child));
                    node.right_child = new_node;
                }
                return;
            } else {
                // Split node
                let middle = node.cells.len() / 2;
                if new_key < node.cells[middle].0 {
                }
                let mut new_internal = Box::new(Node::Internal(InternalNode {
                    parent: node.parent,
                    cells: Vec::new(),
                    right_child: ptr::null_mut(),
                }));
                // TODO
            }
        }

        // Create new root
        let mut root = Box::new(Node::Internal(InternalNode {
            parent: ptr::null_mut(),
            cells: vec![(new_key, self.root)],
            right_child: new_node,
        }));
        self.root = &mut *root as *mut Node;
        mem::forget(node);
    }
}

pub struct Cursor<'a> {
    leaf: &'a mut LeafNode,
    index: usize,
}

impl<'a> Cursor<'a> {
    pub fn get_mut(&mut self) -> Option<&mut u64>
    {
        let index = self.index;
        if index < self.leaf.cells.len() {
            Some(&mut self.leaf.cells[index].1)
        } else {
            None
        }
    }

    pub fn advance(&mut self) {
        if self.index + 1 < self.leaf.cells.len() {
            // Advance within current leaf
            self.index += 1;
        } else if self.leaf.next_leaf != ptr::null_mut() {
            // Advance to next leaf
            self.leaf = unsafe { &mut *self.leaf.next_leaf };
            self.index = 0;
        } else {
            // We reached the end
            self.index = self.leaf.cells.len();
        }
    }
}
