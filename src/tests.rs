use std::mem;
use std::ptr;

use super::{BTree, Node, InternalNode, LeafNode, Cursor,
            bsearch_left, bsearch_right};

#[test]
fn test_bsearch() {
    let array: &[_] = &[(1, 0), (2, 0), (3, 0), (3, 0), (5, 0),
                        (6, 0), (7, 0), (7, 0), (8, 0), (9, 0)];
    assert!(bsearch_right(&array, 0) == 0);
    assert!(bsearch_right(&array, 3) == 4);
    assert!(bsearch_right(&array, 4) == 4);
    assert!(bsearch_right(&array, 6) == 6);
    assert!(bsearch_right(&array, 10) == 10);
    assert!(bsearch_left(&array, 0) == 0);
    assert!(bsearch_left(&array, 3) == 2);
    assert!(bsearch_left(&array, 4) == 4);
    assert!(bsearch_left(&array, 6) == 5);
    assert!(bsearch_left(&array, 10) == 10);
}

fn alloc<T>(v: T) -> *mut T {
    let mut b = Box::new(v);
    let p = &mut *b as *mut T;
    mem::forget(b);
    p
}

fn print_tree_(node_ptr: *mut Node, indent: usize, parent_ptr: *mut Node) {
    fn spaces(indent: usize) {
        for _ in 0..(indent * 2) {
            print!(" ");
        }
    }

    let node = unsafe { &*node_ptr };
    match node {
        &Node::Internal(InternalNode { parent, ref cells, right_child }) => {
            assert!(parent == parent_ptr);
            for (key, node) in cells {
                print_tree_(*node, indent + 1, node_ptr);
                spaces(indent);
                println!("{:?}", key);
            }
            print_tree_(right_child, indent + 1, node_ptr);
        }
        &Node::Leaf(LeafNode { parent, ref cells, .. }) => {
            assert!(parent == parent_ptr);
            for (key, value) in cells {
                spaces(indent);
                println!("{:?} -> {:?}", key, value);
            }
        }
    }
}

fn print_tree(tree: &BTree) {
    print_tree_(tree.root, 0, ptr::null_mut());
}

fn make_tree() -> BTree {
    let leaf1 = alloc(
        Node::Leaf(LeafNode {
            parent: ptr::null_mut(),
            next_leaf: ptr::null_mut(),
            cells: vec![(1, 1), (5, 5), (6, 6)],
        })
    );
    let leaf2 = alloc(
        Node::Leaf(LeafNode {
            parent: ptr::null_mut(),
            next_leaf: ptr::null_mut(),
            cells: vec![(12, 12), (13, 13), (16, 16), (20, 20)],
        })
    );
    unsafe { (*leaf1).leaf().unwrap().next_leaf = (*leaf2).leaf().unwrap(); }
    let root = alloc(
        Node::Internal(InternalNode {
            parent: ptr::null_mut(),
            cells: vec![(12, leaf1)],
            right_child: leaf2,
        })
    );
    unsafe {
        (*leaf1).leaf().unwrap().parent = root;
        (*leaf2).leaf().unwrap().parent = root;
    }
    BTree {
        root: root,
    }
}

#[test]
fn test_search() {
    let mut btree = make_tree();

    {
        let mut cursor = btree.search(6);
        assert!(cursor.index == 2);
        assert!(*cursor.get_mut().unwrap() == 6);
    }
    {
        let mut cursor = btree.search(5);
        assert!(cursor.index == 1);
        assert!(*cursor.get_mut().unwrap() == 5);
    }
    {
        let mut cursor = btree.search(0);
        assert!(cursor.index == 0);
        assert!(*cursor.get_mut().unwrap() == 1);
    }
    {
        let mut cursor = btree.search(12);
        assert!(cursor.index == 0);
        assert!(*cursor.get_mut().unwrap() == 12);
    }
    {
        let mut cursor = btree.search(15);
        assert!(cursor.index == 2);
        assert!(*cursor.get_mut().unwrap() == 16);
    }
    {
        let mut cursor = btree.search(20);
        assert!(cursor.index == 3);
        assert!(*cursor.get_mut().unwrap() == 20);
    }
    {
        let mut cursor = btree.search(32);
        assert!(cursor.index == 4);
        assert!(cursor.get_mut().is_none());
    }
}

fn list_all<'a>(mut cursor: Cursor<'a>) -> Vec<u64> {
    let mut array = Vec::new();
    loop {
        match cursor.get_mut() {
            Some(n) => array.push(*n),
            None => break,
        }
        cursor.advance();
    }
    assert!(cursor.get_mut().is_none());
    array
}

#[test]
fn test_cursor() {
    let mut btree = make_tree();
    let cursor = btree.search(5);
    assert!(cursor.index == 1);
    assert!(list_all(cursor) == [5, 6, 12, 13, 16, 20]);
}

#[test]
fn test_insert() {
    let mut btree = make_tree();
    print_tree(&btree);

    println!("Inserting 4");
    btree.insert(4, 4);
    print_tree(&btree);
    assert!(list_all(btree.search(0)) == [1, 4, 5, 6,
                                          12, 13, 16, 20]);

    println!("Replacing 4");
    btree.insert(4, 42);
    print_tree(&btree);
    assert!(list_all(btree.search(0)) == [1, 42, 5, 6,
                                          12, 13, 16, 20]);

    println!("Inserting 9");
    btree.insert(9, 9);
    print_tree(&btree);
    assert!(list_all(btree.search(0)) == [1, 42, 5, 6, 9,
                                          12, 13, 16, 20]);
}
